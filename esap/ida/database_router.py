from esap.database_router import EsapBaseRouter

class IdaRouter(EsapBaseRouter):
    app_to_db  = {'ida': 'ida'}
