"""
    Business logic for ESAP-gateway.
    These functions are called from the 'views'.
"""

import json
import logging
from ida.models import *
from django.db.models import Q
import django_filters
import collections
from . import Harvester

logger = logging.getLogger(__name__)


def search_facilities(keyword="", objectclass="", facilitytype=None):
    """
    Search the known facilities for a given keyword
    :param keyword: comma separated keywords
    :return:
    """
    return search(Facility, keyword, objectclass, facilitytype=facilitytype)


def search_workflows(keyword="", objectclass="", workflowtype="jupyter-notebook"):
    """
    Search known workflows for a given keyword
    Harvests from the ESCAPE WP3 Zenodo repository and from local database
    :param keyword: comma separated keywords
    :return:
    """
    response = {}
    response["description"] = "ESAP API Gateway"
    response["requested_page"] = "1"
    response["requested_page_size"] = None
    response["max_page_size"] = 500
    response["default_page_size"] = "ESAP API Gateway"
    response["count"] = 0
    response["pages"] = 1
    response["results"] = []

    from django.core import serializers
    db_workflows = serializers.serialize("python", Workflow.objects.filter(workflowtype=workflowtype))
    for db_entry in db_workflows:
        response["results"].append(db_entry["fields"])
    zenodo_workflows = Harvester.get_data_from_zenodo(query=keyword)
    response["results"].extend(zenodo_workflows)
    return response



def search(model, keyword="", objectclass="", **kwargs):
    """
    Search the known facilities for a given keyword
    :param keyword: comma separated keywords
    :return:
    """

    results = []
    try:
        if not keyword and not kwargs: # No keyword or additional param passed in, get all objects
            results = model.objects.all()

        else:

            if objectclass.lower()=="workflow":
                results = model.objects.filter(
                    Q(name__icontains=keyword) | Q(description__icontains=keyword) | Q(url__icontains=keyword)
                )

            if objectclass.lower()=="facility":
                facilitytype = kwargs.get("facilitytype", None)
                if not keyword: # Filter by just facilitytype
                    results = model.objects.filter(
                        facilitytype=facilitytype
                    ) if facilitytype else model.objects.all()
                else:
                    # Filter by just facilitytype and keyword, or just keyword if no facilitytype
                    results = model.objects.filter(
                        Q(Name__icontains=keyword) | Q(description__icontains=keyword) | Q(url__icontains=keyword),
                        facilitytype=facilitytype
                    ) if facilitytype else model.objects.filter(Q(name__icontains=keyword) | Q(description__icontains=keyword) | Q(url__icontains=keyword))

    except Exception as error:
        record = {}
        record['result'] =  str(error)
        results.append(record)
        return results
    return results
