class EsapBaseRouter:
    # Mapping of app label to database name
    # Define in subclass
    app_to_db = dict()

    def db_for_read(self, model, **hints):
        return self.app_to_db.get(model._meta.app_label)

    def db_for_write(self, *args, **kwargs):
        return self.db_for_read(*args, **kwargs)

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == self.app_to_db.get(app_label):
            return True
