
title = "Science Data Centre (KIS) Archive"
logo = "https://sdc.leibniz-kis.de/typo3conf/ext/kis_template/Resources/Public/Images/logo_kis_rz.svg"

# definition of the query
query_schema = {
    "name": "sdc_kis",
    "title": "Science Data Centre (KIS) Archive",
    "type": "object",
    "properties": {
        "catalog": {
            "type": "string",
            "title": "Catalog",
            "default": "sdc_kis",
            "enum": ["sdc_kis"],
            "enumNames": ["Science Data Centre (KIS)"]
        },
        "instrument": {
            "type": "string",
            "title": "Instruments",
            "default": "gris",
            "enum": ["gris", "lars", "chrotel"],
            "enumNames": ["GRIS@GREGOR", "LARS@VTT", "ChroTel"],
        },
        "begin-date": {
            "type": "string",
            "title": "Observation Start Date",
        },
        "end-date": {
            "type": "string",
            "title": "Observation End Date",
        },
        "begin-time": {
            "type": "string",
            "title": "Observation Start Time",
        },
        "end-time": {
            "type": "string",
            "title": "Observation End Time",
        },

    },
    "required": ["catalog", "instrument", "begin-date"],
    "dependencies": {
        "instrument": {
            "oneOf": [
                {
                    "properties": {
                        "instrument": {"enum": ["gris"]},
                        "begin-position-theta": {
                                    "type": "number",
                                    "title": "Begin Heliocentric Angle [theta]",
                                },
                                "end-position-theta": {
                                    "type": "number",
                                    "title": "End Heliocentric Angle [theta]",
                                },
                                "begin-position-mu": {
                                    "type": "number",
                                    "title": "Begin Heliocentric Angle [mu]",
                                },
                                "end-position-mu": {
                                    "type": "number",
                                    "title": "End Heliocentric Angle [mu;]",
                                },
                                "target": {
                                    "type": "string",
                                    "title": "Target",
                                },
                    }
                },
                {
                    "properties": {
                        "instrument": {"enum": ["lars"]},

                    }
                },
                {
                    "properties": {
                        "instrument": {"enum": ["chrotel"]},

                    }
                },
            ]
        }
    },
}
ui_schema = {"begin-date": {"ui:help": "*required (e.g. 2016-05-15)", "ui:placeholder": "YYYY-MM-DD"},
             "end-date": {"ui:placeholder": "YYYY-MM-DD (optional)"},
             "begin-time": {"ui:placeholder": "HH:MM (optional)"},
             "end-time": {"ui:placeholder": "HH:MM (optional)"},
             "begin-position-theta": {"ui:placeholder": "[0<theta<90] (optional)"},
             "end-position-theta": {"ui:placeholder": "[0<theta<90] (optional)"},
             "begin-position-mu": {"ui:placeholder": "[0<mu<1] (optional)"},
             "end-position-mu": {"ui:placeholder": "[0<mu<1] (optional)"},
             "target": {"ui:placeholder": "e.g. Sunspot, Quiet Sun (optional)"}
             }

