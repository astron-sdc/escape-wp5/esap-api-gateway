from esap.database_router import EsapBaseRouter

class RucioRouter(EsapBaseRouter):
    app_to_db = {'rucio': 'rucio'}
