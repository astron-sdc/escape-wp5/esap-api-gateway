"""SDC KIS Service Connector for ESAP."""

from rest_framework import serializers
from .query_base import query_base
import logging
import requests
from dateutil import parser
from datetime import datetime, timedelta

#import urllib3
#urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger(__name__)

def converttodt(datestring: str):
    '''Convert any string to a datetime object'''
    return parser.parse(datestring)


def findindex(onearray, value):
    '''Find index of value in the onearray'''
    minarray = np.abs(onearray - value)
    return minarray.argmin()

def createdatestring(dt_string):
    """
    Get the start and end datestring for a selected date

    :param dt_string:
    :return:
    """
    if dt_string.hour:
        if dt_string.minute:
            dt_string_iso = dt_string.isoformat() + 'Z'
        else:
            dt_string_iso = dt_string.isoformat() + 'Z'
    else:
        dt_string_iso = dt_string.isoformat() + 'Z'

    return dt_string_iso



def daterange(dt_string):
    """
    Get the start and end datestring for a selected date

    :param dt_string:
    :return:
    """
    if dt_string.hour:
        if dt_string.minute:
            regx_gt = dt_string.isoformat() + 'Z'
            regx_lt = (dt_string + timedelta(minutes=1)).isoformat() + 'Z'
        else:
            regx_gt = dt_string.isoformat() + 'Z'
            regx_lt = (dt_string + timedelta(hours=1)).isoformat() + 'Z'
    else:
        regx_gt = dt_string.isoformat() + 'Z'
        regx_lt = (dt_string + timedelta(1)).isoformat() + 'Z'

    return regx_gt, regx_lt

class Client:
    """
    Remote access client for SDC data

    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self):
        self.url = 'https://readuser:Maun45!@restheart.sdc.leibniz-kis.de/'
        #self.url = 'http://dockertest:8083/sdc/'
        self.obs_path = '_observations'
        self.headers = {"content-type": "application/json"}
        self.qpreamble = 'description'

    def createquery(self, instrument='gris', regex=None, key=None, sortkey=None):
        """

        Main Restheart API query

        :param regex:
        :param key:
        :return:
        """
        search_prefix = self.url + instrument + self.obs_path + '?filter='
        sort_suffix = '&sort={"' + self.qpreamble + '.' + sortkey + '":1}'
        if type(key) ==list:
            combined_query = []
            for ik,ikey in enumerate(key):
                if ikey=='DATE_BEG':
                    qstring={}
                    qstring[self.qpreamble + '.' + ikey] = {f: {'$date': regex[ik][f]} for f in regex[ik]}
                    combined_query.append(qstring)
                else:
                    qstring={}
                    qstring[self.qpreamble + '.' + ikey]  = {f: regex[ik][f] for f in regex[ik]}
                    combined_query.append(qstring)
            search_string = search_prefix + '{"$and":' + str(combined_query) + '}'+sort_suffix
        else:
            search_string = search_prefix +'{"' + self.qpreamble + '.' + key + '":' + str(
                {f: {'$date': regex[f]} for f in regex}) + '}'+sort_suffix

        return requests.get(search_string, headers=self.headers, verify=False)

    def getresults(self, query, fullheader=False):
        """
        Prints info for the given search query

        :param query:
        :param fullheader:
        :return:
        """

        # Begin date is optional
        if 'begin-date' in query.keys():
            # End time is optional, if present add to end date
            # Begin time is optional, if present add to begin date
            if 'begin-time' in query.keys():
                begin_time = converttodt(query['begin-time'])
                begin_date = createdatestring(
                    converttodt(query['begin-date']) + timedelta(hours=begin_time.hour, minutes=begin_time.minute))
            else:
                begin_date = createdatestring(converttodt(query['begin-date']))

            # Begin time is optional, if present add to begin date
            if 'begin-time' in query.keys():
                begin_time = converttodt(query['begin-time'])
                begin_date = createdatestring(converttodt(query['begin-date']) + timedelta(hours=begin_time.hour,minutes=begin_time.minute))

        # End date is optional, if not provided the end date is +1 day after the begin date
        if 'end-date' in query.keys():
            # End time is optional, if present add to end date
            if 'end-time' in query.keys():
                end_time = converttodt(query['end-time'])
                end_date = createdatestring(
                    converttodt(query['end-date']) + timedelta(hours=end_time.hour, minutes=end_time.minute))
            else:
                end_date = createdatestring(converttodt(query['end-date']))
        else:
            end_date = createdatestring(converttodt(query['begin-date']) + timedelta(1))

        query_rekey = ['DATE_BEG']
        query_regex = [{'$gt': begin_date, '$lt': end_date}]

        # theta
        list_theta =  list(set(['begin-position-theta', 'end-position-theta']).intersection(query.keys()))
        print(list_theta)
        if list_theta:
            query_rekey.append('THETA')
            query_regex.append(
                {'$gt': float(query['begin-position-theta']) if 'begin-position-theta' in list_theta and query['begin-position-theta']!='undefined' else 0,
                 "$lt": float(query['end-position-theta'])   if 'end-position-theta' in list_theta   and query['end-position-theta']!='undefined'   else 90
                 }
            )

        # mu
        list_mu = list(set(['begin-position-mu', 'end-position-mu']).intersection(query.keys()))
        if list_mu:
            query_rekey.append('MU')
            query_regex.append(
                {'$gt': float(query['begin-position-mu']) if 'begin-position-mu' in list_mu and query['begin-position-mu']!='undefined' else 0.0,
                 "$lt": float(query['end-position-mu']) if 'end-position-mu' in list_mu and query['end-position-mu']!='undefined' else 1.0
                 }
            )

        # target
        if 'target' in query.keys():
            if query['target']!='undefined':
                query_rekey.append('TARGET')
                query_regex.append( {'$regex':f".*{query['target']}.*"})

        response = self.createquery(instrument=query['instrument'],
                                    regex= query_regex,
                                    key=query_rekey,
                                    sortkey='DATE_BEG')

        result = []

        #The new api does not return a dict with _returned value
        #for iobs in range(response.json()['_returned']):
        for iobs in range(len(response.json())):
            if fullheader:
                pprint(response.json()[iobs])
            else:
                result_iobs={}
                result_iobs["date"] = str(datetime.utcfromtimestamp(
                    response.json()[iobs]['description']['DATE_BEG']['$date'] / 1000.).strftime('%Y-%m-%d'))

                result_iobs["time_begin"] = str(datetime.utcfromtimestamp(
                    response.json()[iobs]['description']['DATE_BEG']['$date'] / 1000.).strftime('%H:%M:%S'))

                result_iobs["time_end"] = str(datetime.utcfromtimestamp(
                    response.json()[iobs]['description']['DATE_END']['$date'] / 1000.).strftime('%H:%M:%S'))

                result_iobs["target"] = response.json()[iobs]['description']['TARGET']
                result_iobs["wave_min"] = "{:5.2f}".format(response.json()[iobs]['description']['WAVELENGTH_MIN'])
                result_iobs["wave_max"] = "{:5.2f}".format(response.json()[iobs]['description']['WAVELENGTH_MAX'])

                result_iobs["mu"] =  "{:5.2f}".format(response.json()[iobs]['description']['MU'])
                result_iobs["theta"] = "{:5.2f}".format(response.json()[iobs]['description']['THETA'])
                result_iobs["filter"] = response.json()[iobs]['description']['FILTER']
                result_iobs["oid"] = response.json()[iobs]['_id']['$oid']

                result_iobs["link"] = "https://archive.sdc.leibniz-kis.de/SDCDetailServlet?Instrument={:s}&ObjectId={:s}".format(query['instrument'],response.json()[iobs]['_id']['$oid'])

                # For helioviewer API
                centerX = (response.json()[iobs]['description']['HPLN_TAN_MIN'] + response.json()[iobs]['description']['HPLN_TAN_MAX'])/2.
                centerY = (response.json()[iobs]['description']['HPLT_TAN_MIN'] + response.json()[iobs]['description'][
                    'HPLT_TAN_MAX'])/2.
                imageScale = response.json()[iobs]['description']['S_RESOLUTION']
                sdate = datetime.utcfromtimestamp(response.json()[iobs]['description']['DATE_BEG']['$date']/1000.).isoformat()

                result_iobs["helio"] = f"https://helioviewer.org/?date={sdate}&imageScale={imageScale}&centerX={centerX}&centerY={centerY}"#&imageLayers=[SDO,HMI,continuum,1,100,0,60,1,{sdate}]"
                result.append(result_iobs)

        return result



class sdc_kis_connector(query_base):
    """A connector to query SDC KIS archives"""

    # construct a query for the SDC KIS REST API
    def construct_query(self, dataset, esap_query_params, translation_parameters):

        query = {}
        where = {}
        error = {}

        for ikeys in esap_query_params.keys():
            query[ikeys] = str(esap_query_params[ikeys][0])

        desired_value = "undefined"
        for key, value in query.items():
            if value == desired_value:
                del query[key]
                break

        return query, where, error

    def run_query(
        self,
        dataset,
        dataset_name,
        query,
        session,
        override_access_url=None,
        override_service_type=None,
    ):
        """
        :param dataset: the dataset object that must be queried
        :param query_params: the incoming esap query parameters)
        :return: results: an array of dicts with the following structure;
        """
        sdc_client = Client()
        result = sdc_client.getresults(query)
        return result


    class CreateAndRunQuerySerializer(serializers.Serializer):
        date = serializers.CharField()
        time_begin = serializers.CharField()
        time_end = serializers.CharField()

        target = serializers.CharField()
        wave_min = serializers.CharField()
        wave_max = serializers.CharField()
        mu = serializers.CharField()
        theta = serializers.CharField()
        filter = serializers.CharField()
        oid = serializers.CharField()
        link = serializers.CharField()
        helio = serializers.CharField()