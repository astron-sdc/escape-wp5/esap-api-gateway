# ESAP GUI Gateway
ESAP API Gateway is a 'backend' web application written in Django.
It provides a range of services that can be accessed through a REST API.

## Documentation

* https://git.astron.nl/astron-sdc/esap-api-gateway/-/wikis/home

## Contributing

For developer access to this repository, please send a message on the [ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
